---
title: Introduction à Git
author: Arthur Bouché, Thibaud Toullier
email:	arthur.bouche@univ-eiffel.fr, thibaud.toullier@univ-eiffel.fr
date: 	Mardi 31 octobre 2023
theme: solarized
highlightTheme: stackoverflow-dark
previewLinks: true
mathjax: true
enableMenu: true
preprocessor: script/preproc.js
css: ./css/font-awesome-4.7.0/css/font-awesome.min.css,./node_modules/reveal.js-plugins/customcontrols/style.css,./node_modules/reveal.js-plugins/chalkboard/style.css,node_modules/reveal.js-copycode/plugin/copycode/copycode.css,./css/presentation.css
scripts: node_modules/reveal.js-mermaid-plugin/plugin/mermaid/mermaid.js,node_modules/reveal.js-copycode/plugin/copycode/copycode.js,node_modules/reveal.js-menu/menu.js,node_modules/reveal.js-plugins/customcontrols/plugin.js,node_modules/reveal.js-plugins/chalkboard/plugin.js,plugin/enable-plugins.js
revealOptions:
  transition: 'fade'
  width: 1600
  height: 1200
  slideNumber: 'c/t'
---

# Introduction à Git
<div class="author">Arthur Bouché<br>
<a href="mailto:arthur.bouche@univ-eiffel.fr">arthur.bouche@univ-eiffel.fr</a><br>
Ingénieur Recherche - Univ. Gustave Eiffel
</div>
<div class="author">Thibaud Toullier<br>
<a href="mailto:thibaud.toullier@univ-eiffel.fr">thibaud.toullier@univ-eiffel.fr</a><br>
Ingénieur Recherche - Univ. Gustave Eiffel
</div>


<img src="img/logo_univ_gustave_eiffel_rvb.svg" width="300px" style="position:absolute;margin:0;padding:0;bottom:-400px;"/>



# Cela vous est-il déjà arrivé ?

![](img/git-training.gif)


## Plus maintenant ! (Ou presque...)


<div class="container">
<div class="col">

**Voyons ensemble dès maintenant comment profiter de Git**

</div>
<div class="col">

![](img/git.png)

</div>
</div>

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Objectifs</h4>

- Comprendre ce qu'est un logiciel de contrôle de version,
- Mettre votre travail sous contrôle de version
- Apprendre à utiliser les fonctionnalités de base de Git,
- Comprendre le principe d'une forge logicielle.
- Commencer à travailler en collaboration

</div>

Note: Enter speaker notes here.

# Sommaire

- [Les sytèmes de gestion de versions](#/3)
- [C'est quoi Git et pourquoi Git ?](#/4)
- [Premiers pas avec Git](#/5)
- [Cas pratique](#/6)
- [Utiliser une interface graphique](#/7)
- [Travail collaboratif](#/8)
- [Des questions ?](#/9)

Diapos disponibles en ligne <https://ttoullie.gitlabpages.inria.fr/git-training/>


# Les sytèmes de gestion de versions


## Définitions

Système de versions concurrentes (SVC, *CVS en anglais*)
- La gestion de versions permet de **gérer de multiples versions** d’un document.
- Ces outils sont utilisables lors de **travail en équipe**, et lors de **travail individuel**

<br><br>

Exemples de systèmes : CVS, Mercurial, Bazaar, SVN, et Git
- CVS et SVN utilisent un **serveur central** pour synchroniser les documents suivis
- Mercurial et Git sont **décentralisés** : les développeurs peuvent travailler ensemble sans accès au serveur
- CVS et SVN sont de moins en moins utilisés en faveur de Git


## Pourquoi utiliser un système de gestion de version ?

- Offre la possibilité de visualiser l’historique des modifications sur un projet, et de revenir à une version précise
- Rend accessible plusieurs versions d’un projet : une ancienne version, une version en production, et une version en développement
- Lors de travail en équipe, la possibilité d’éditer le même fichier en même temps.


# C'est quoi Git et pourquoi Git ?

Note:
Avant de commencer, parlons un peu de ce qu'est Git et de comment il diffère de certaines autres formes de contrôle de source. Je vais faire quelques comparaisons avec Subversion, mais des comparaisons similaires peuvent être appliquées à d'autres systèmes de contrôle de source également.

## Introduction

<div class="container">
<div class="col">

- Qu’est ce GIT ?
    - Un logiciel de gestion de versions décentralisé.
    - Un logiciel open source (GPLv2)
    - Créé en 2005 par Linus Torvalds
    - Objectif remplacer CVS et SVN

<br><br>

- Git & Linux 6.6
    - 1,217,245 commits, sur 81,766 fichiers
    - 27,378 contributeurs

</div>
<div class="col">
![Linus Torvalds](img/torvalds.png "Linus Torvalds")
</div>
</div>


## Instantanés!

<div class="container">
<div class="col">

**Subversion et autres SVC traditionnels**

Les informations consistent en un ensemble de fichiers + **des deltas** de changements au fil du temps.

</div>
<div class="col">

**Git**

Les informations consistent en un ensemble d'***instantanés***. À chaque modification validée, Git prend un autre snapshot.

</div>
</div>

<div class="container">
<div class="col">

![](img/deltas.png)


</div>
<div class="col">

![](img/snapshots.png)

</div>
</div>

<div class="alert alert-info">

**En gardant des instantanés des changements au fil du temps, cela permet à Git d'effectuer la plupart des opérations en local.**

</div>

Note:
Dans un système comme Subversion, les changements sont suivis comme une série de modifications au fil du temps. Lors qu'une modification est validée, c'est le delta que SVN gardera en mémoire.
Avec Git, vos modifications sont des captures d'écran de ce qui s'est passé. C'est une version complète du fichier modifié - pas seulement les changements qui y ont été apportés. Git agit un peu comme un système de fichiers avec des outils en plus.
C'est une distinction importante entre Git et presque tous les autres systèmes de contrôle de version. Cela amène Git à reconsidérer presque tous les aspects du contrôle de version que la plupart des autres systèmes ont copié de la génération précédente. Cela fait de Git quelque chose de plus semblable à un mini système de fichiers avec des outils incroyablement puissants construits par-dessus, plutôt que simplement un VCS. Nous explorerons certains des avantages que vous obtenez en pensant à vos données de cette manière lorsque nous aborderons les branches Git dans la section sur le Branchage Git.



# Premiers pas avec Git


## Installation

**Installation sous Linux (Debian / Ubuntu)**

``` shell
$ apt install git
$ apt install git-gui
```

**Installation sous Windows**

- <https://git-scm.com/>
- Télécharger "64-bit Git for Windows Setup"


## Documentation Git

Git est extrêmement **puissant** et offre de nombreuses *fonctionnalités sophistiquées*.

Par conséquent, la 📚 [documentation](https://git-scm.com/docs) est très **complète**, mais peut souvent être **accablante** 🤯 lorsque vous débutez.

**Nous sommes là pour vous guider !**

- [https://git-scm.com/docs](https://git-scm.com/docs) : documentation complète
- `git <command> --help` : pages de manuel de `git`
- <https://git-scm.com/book/fr/v2> : livre d'introduction, dense mais très **didactique**


## Configuration initiale de Git

Utilisation de la commande `config`.

Git doit connaître votre identité (Nom et Email) pour vous identifier dans le projet
et l’ajouter dans les metadatas de vos commits

``` shell
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

<br><br><br>
<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Note</h4>

- À noter l'utilisation de l'argument `--global`, qui va configurer les données utilisateur sur tous vos projets (`~/.gitconfig`). Il est possible de le faire pour un projet spécifique (`.git/config`).

- D'autres choses peuvent être configurées comme l'éditeur par défaut par exemple (`git config --global core.editor emacs`)

</div>


## Les trois états

<div class="container">
<div class="col">
Git gère **3 états** dans lesquels les fichiers peuvent résider : modifié, indexé et validé.

- **Modifié** : vous avez modifié le fichier mais qu’il n’a pas encore été validé en base.
- **Indexé** : signifie que vous avez marqué un fichier modifié dans sa version actuelle pour qu’il fasse partie du prochain instantané du projet.
- **Validé** : signifie que les données sont stockées en sécurité dans votre base de données locale.

$\implies$ **3 sections** principales d’un projet Git 
</div>
<div class="col">

![](img/three_states.png "Les actions effectuées dans un projet Git : modifier, indexer et valider.")
</div>
</div>

<br>

<div class="alert alert-warning">
<h4><i class="fa fa-warning"></i>Infos</h4>
Toutes les commandes Git opèrent sur ces trois états.
</div>


## Les trois sections

<div class="container">
<div class="col">

- **Dossier de travail** : Une version du projet qui est extraite et placée sur le disque. Ses fichiers sont extraits de la base de données compressée dans le répertoire `.git`.
- **L’index GIT (zone de préparation des commits)** : Un fichier, dans le répertoire `.git`, qui stocke des informations sur ce qui sera inclus dans votre prochain commit.
- **Le dépôt (`.git`)** : métadonnées et la base de données d'objets du projet.

</div>
<div class="col">

![](img/three_states.png "Répertoire de travail, zone d’index et répertoire Git.")
</div>
</div>

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Infos</h4>
La plupart des opérations de Git ne nécessitent que des fichiers et ressources locaux — généralement aucune information venant d’un autre ordinateur du réseau n’est nécessaire. 

- Accès à tout l'historique (même hors-ligne)
- Très peu de choses que vous ne puissiez réaliser hors-ligne

</div>

Note:
L’utilisation standard de Git se passe comme suit :
<ul class="org-ul">
<li>vous modifiez des fichiers dans votre répertoire de travail ;</li>
<li>vous indexez les fichiers modifiés, ce qui ajoute des instantanés de ces fichiers dans la zone d’index ;</li>
<li>vous validez, ce qui a pour effet de basculer les instantanés des fichiers de l’index dans la base de données du répertoire Git.</li>
</ul>


## Création d’un dépôt Git dans un dossier de travail

Votre premier dépôt Git avec la commande `init`!

```shell
$ git init
```

Un dossier `.git` sera alors créé


## Structure du dossier `.git`

Résultat : (`tree .git`)

```shell
.git
    ├── branches
    ├── config
    ├── description
    ├── HEAD
    ├── hooks
    ├── info
    │   └── exclude
    ├── objects
    │   ├── info
    │   └── pack
    └── refs
        ├── heads
        └── tags
```

## Architecture (1/2)

**`.git` contient 4 fichiers**

<pre><code class="shell hljs language-shell" data-trim data-noescape data-line-numbers="|1-2|1,3|1,4|1,5" data-fragment-index="1">
.git
    ├── config
    ├── description
    ├── HEAD
    ├── index
    ├── ...
</code></pre>

<ul>
<li class="fragment fade-in-then-semi-out" data-fragment-index="1"><code>.git/config</code> - Ce fichier contient la configuration du dépôt Git, comme les informations sur l'utilisateur, les alias de commandes, les dépôts distants, etc.</li>
<li class="fragment fade-in-then-semi-out" data-fragment-index="2"><code>.git/description</code> - Ce fichier contient la description du projet</li>
<li class="fragment fade-in-then-semi-out" data-fragment-index="3"><code>.git/HEAD</code> - Ce fichier pointe vers la branche actuelle sur laquelle vous vous trouvez dans le dépôt.
<li class="fragment fade-in-then-semi-out" data-fragment-index="4"><code>.git/index</code> - La fameuse "staging area" (non existante l'instant!)
</ul>


## Architecture (2/2)

**`.git` contient 4 dossiers**


<pre><code class="shell hljs language-shell" data-trim data-noescape data-line-numbers="|1,3|1,4-5|1,6-8|1,9-11" data-fragment-index="1">
.git
    ├── ...
    ├── hooks
    ├── info
    │   └── exclude
    ├── objects
    │   ├── info
    │   └── pack
    └── refs
        ├── heads
        └── tags
</code></pre>
<ul>
	<li class="fragment fade-in-then-semi-out" data-fragment-index="1"><code>.git/hooks</code> - Les hooks sont des scripts exécutés à des points spécifiques de la vie du dépôt Git, tels que pre-commit (avant la validation), post-commit (après la validation), etc.</li>
	<li class="fragment fade-in-then-semi-out" data-fragment-index="2"><code>.git/info</code> - Ce dossier contient des informations spécifiques au dépôt local qui ne sont pas partagées avec d'autres collaborateurs du dépôt.</li>
	<li class="fragment fade-in-then-semi-out" data-fragment-index="3"><code>.git/objects</code> - Ce dossier stocke tous les objets du dépôt Git, tels que les commits, les trees (arborescences) et les blobs (contenus des fichiers). Chaque objet est identifié par un hachage **`SHA-1`** basé sur son contenu.</li>
	<li class="fragment fade-in-then-semi-out" data-fragment-index="4"><code>.git/refs</code> - Ce sous-dossier contient des références vers des commits spécifiques. Deux types de références :
		<ul>
			<li><code>refs/heads/</code> - Contient des pointeurs vers les branches.</li>
			<li><code>refs/tags/</code> - Contient des pointeurs statiques vers des points spécifiques de l'historique, généralement utilisés pour marquer des versions (tags).</li>
		</ul>
	</li>
</ul>


Note:
`[déprécié] .git/branches` - Plus utilisé, à propos de sa création au `git init` : "There is not enough justification for doing this. We do not update things in `.git/branches` and `.git/remotes` anymore, but still do read information from there and will keep doing so."


# Cas pratique


## Créer son dépôt git

Pour créer sont dépot utiliser la commande :

```shell
$ git init
```

Un dossier `.git` sera alors créé


## Son premier commit

Créer 2 fichier LICENSE et README  

<br>

Voir l'état du dépôt (état **modifié**):

```shell
$ git status
```

`git add` permet d'indexer des fichier pour une future validation (commit) :

```shell
$ git add LICENSE README
```

Voir l'état du dépôt (état **indexé**):

```shell
$ git status
```

`git commit` permet de valider les fichier indexés. `-m` permet d'ajouter
un message à son commit

```shell
$ git commit -m "mon premier commit"
```

Voir l'état du dépôt (état **validé**):

```shell
$ git status
```

La commande `git log` permet de voir plus d'informations sur le dépôt

```shell
$ git log --stat --summary
```


## Note sur les commits

![](img/xkcd.png "Extrait de xkcd.com")
<div class="container">
<div class="col">

<div class="alert alert-success">
<h4><i class="fa fa-check"></i>A faire</h4>

- Gardez les modifications des commits aussi petites que possible.
- Décrivez ce qui a été modifié et pourquoi.
- Limitez la première ligne à 50 caractères.
- Insérez une ligne vide après la première ligne.
- Enveloppez les lignes suivantes à 72 caractères.

</div>
</div>
<div class="col">

<div class="alert alert-warning">
<h4><i class="fa fa-warning"></i>A ne pas faire</h4>

- Mélanger les changements d'espaces avec des changements de code fonctionnel.
- Mélanger des changements de code fonctionnel non liés.
- Envoyer une grande fonctionnalité en un seul commit.

</div>
</div>
</div>


## Modification fichier

Modifier le fichier LICENSE puis utiliser la commande suivante :

```shell
$ git status
```

Elle indique sur quelle branche vous êtes (on y reviendra plus tard) et
les fichier qui sont modifiés, indexés et non suivis

```shell
$ git add LICENSE
$ git commit -m "modification du fichier LICENSE"
```

Utiliser de nouveau la commande `git log` pour voir le statut de votre
git.

```shell
$ git log --stat --summary
```

Si vous voulez chercher les modifications d'un seul fichier (LICENSE par
exemple) vous pouvez utiliser la commande suivant :
</p>

```shell
$ git log --stat --summary LICENSE
```

Entrainez-vous à créer des fichiers et dossiers et les valider!

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Note</h4>
La commande `git show` est une autre option très versatile pour avoir
des informations sur un commit, ou un fichier, ou un tag ou tout autre objet !

`git show --help` 😉
</div>


## Les gitignore

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Et si&#x2026;</h4>
Et si je souhaite ignorer un fichier ?
</div>

- **Objectif :** ne pas suivre certains fichiers
    - fichiers de logs, de compilation, pdf générés, *etc.*
- On peut ignorer un dossier entier

Exemple de `.gitignore` où l'**astérisque** désigne toute suite de caractères :

``` shell
*~
*.o
*.moc.cpp
*.so*
*.pdf
build/
**/__pycache__/*
```

Note:
Dans le cas de programme par exemple, lors de la compilation, des
fichier <code>.o</code> sont généré. Ces fichier n'ont pas vocation à être commiter
sur le depot git car ils seront modifiés à chaque compilation. Si ne
veux pas qu'ils soient présent lors de la commande <code>git status</code> nous
pouvons créer des fichiers <code>.gitignore</code>.<br>


## A vous !

Créer un fichier toto.o et faites un

``` shell
$ git status
```

Créer maintenant un fichier .gitignore avec dedans `*.o` et refaites un

``` shell
$ git status
```

Les modifications du fichier `toto.o` ne sont plus visible.


## D'autres fichiers particuliers

- `.gitkeep` : permet de s'assurer de la présence d'un dossier, y compris vide
- `.gitmodules` : inclure des sous-modules / projets `Git` dans un projet `Git`
    - Exemple de =.gitmodules =:
        
            [submodule "modules/mon-projet"]
                    path = modules/mon-projet
                    url = git@gitlab.com:ttoullie/mon-projet.git


## Revenir à une version précédente du git (1/3)

<div class="container">
<div class="col">

![](img/head-to-master.png)

</div>
<div class="col">

Voici à quoi ressemble votre dépot git :

- `master` est le nom de la branche
- `HEAD` le commit (validation) d'où vous travaillez

</div>
</div>

Lors de la command `git log`, chaque commit à un numéro unique qui permet 
de l'identifier : 

``` shell
$ commit dab6354921766d...
```

`git checkout` permet de changer la `HEAD` de commit en cours :

``` shell
$ git checkout id-first-commit
```

## Revenir à une version précédente du git (2/3)

<br><br>

- Identifiez votre premier commit à l'aide de `git log`
- Basculez sur ce commit avec `git checkout`

<br><br>
<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Infos</h4>

Si des fichier n'ont pas été commit, un message va apparaître pour vous
dire de les valider avant de réaliser le `git checkout`.

</div>


## Revenir à une version précédente du git (3/3)

<div class="container">
<div class="col">

![](img/head-to-first.png)

</div>
<div class="col">

Voici ci-dessus l'état de votre dépot à présent :

Si vous regardez le contenu de votre dossier, vous allez
remarquer qu'il est revenu dans l'état où il était lors du premier
commit.

La commande `git log` vous le confirmera.

</div>
</div>

Pour revenir au dernier commit réalisé ils vous suffit de faire la
commande suivante :

``` shell
$ git checkout master
```

`master` correspond à votre branch principale de votre dépot git. 


## Gestion des branches (1/3)

Dans les bonnes pratiques, lors du développement
d'un projet sous Git, le code de la branche principale (`master`) est
**toujours fonctionnel**. 

<div class="alert alert-warning">
<h4><i class="fa fa-warning"></i>Bonnes pratiques</h4>
En pratique, on évite de travailler directement sur la branche `master` !
</div>

<div class="alert alert-success">
<h4><i class="fa fa-check"></i>Git power!</h4>
💪 c'est là la force de `Git`, grâce à son fonctionnement, créer une branche, c'est juste une opération de 41 caractères ! (40 caractères de SHA et 1 retour chariot). A l'inverse d'autres SVC, où on essaie de faire le moins de branches possibles !
</div>

<div class="container">
<div class="col">
Ainsi, pour effectuer des développements, nous
créons une branche parallèle à la branche principale (branche de
développement), effectuons nos commits sur cette nouvelle branche, et
une fois que les développements sont fonctionnels, nous les fusionnons
avec la branche principale (`master`).
</div>
<div class="col">

![](img/git_diagram_merge_transparent.svg)

</div>
</div>


## Gestion des branches (2/3)

<div class="container">
<div class="col">
Exemple de "workflow": 

- Version **stable** (`master`)
    contient la version stable **la plus à jour du projet**
- Version en **développement** (`develop`)
    contient la version instable la plus à jour du projet. Cette branche prépare la **future version du projet**
- Ajout de fonction (`feature.xxx`)
    chaque branche contient **une évolution importante** du projet. *Ces branches seront fusionnées dans develop*
- Correction de bug impactant la production (`hotfix.xxx`)
    branches courtes de **correctifs** qui seront fusionnées dans *master et develop*

</div>
<div class="col">
![](img/branches.png)
</div>
</div>


## Gestion des branches (3/3)

Pour voir les branches existantes :

``` shell
$ git branch
```

<div class="container">
<div class="col">
**Création d'une branche**

Tout d'abord, se mettre sur la branche master

``` shell
$ git checkout master
```

Puis, pour créer la branche `testing` :

``` shell
$ git branch testing
```

</div>
<div class="col">
![](img/head-to-master-test.png)
</div>
</div>

<hr>

<div class="container">
<div class="col">
Regardez la liste des branches

``` shell
$ git branch 
```

Aller sur la branche `testing` : 

``` shell
$ git checkout testing
```

</div>
<div class="col">
![](img/head-to-testing.png)
</div>
</div>


## Fusionner les branches

**Dans cette nouvelle branche `testing`**, ajoutez un nouveau fichier, et validez-le.

``` shell
$ echo "contenu du fichier de testing" > fichier_testing
$ git add fichier_testing
$ git commit -m "nouveau fichier de testing"
```

Maintenant, fusionnons la branche `testing` dans `master`

``` shell
$ git checkout master # retour sur master
$ git merge --no-ff testing # fusion de la branche testing sur master
```

Regardez les logs: 

``` shell
$ git log --stat --summary
```

On peut maintenant supprimer la branche `testing` !

``` shell
$ git branch -d testing
```

![](img/git_example_merge_transparent.svg)


## Type de fusion

![](img/fast-forward.png "Types de fusion, différence avec 'fast-forward'")


## Fusion avec conflit (1/4)

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Et si&#x2026;</h4>
Et si deux fichiers ont été modifiés en même temps sur deux branches différentes ?
</div>

Créez une nouvelle branche `conflit`

``` shell
$ git branch conflit
```

**EN RESTANT DANS master**, modifiez le fichier `LICENSE`

``` shell
$ echo "Modifié dans master" > LICENSE
$ git add LICENSE
$ git commit -m "modification de license depuis master"
```

Allez sur `conflit` et faîtes de même (mais pas la même modification!)

``` shell
$ git checkout conflit
$ echo "Modifié dans conflit" > LICENSE
$ git add LICENSE
$ git commit -m "modification de license depuis conflit"
```

## Fusion avec conflit (2/4)

Maintenant, fusionnez `conflit` dans `master`

``` shell
$ git checkout master
$ git merge --no-ff conflit
```

Regardez le statut de votre dépôt :

``` shell
$ git status
```

<div class="alert alert-warning">
<h4><i class="fa fa-warning"></i>Conflit !</h4>
Git ne peut pas fusionner !
Heureusement, il vous propose de résoudre le conflit pour ensuite fusionnner!
</div>


## Fusion avec conflit (3/4)

- Quand le merge ne marche pas !
    - Git laisse le reste du travail à l’utilisateur et ne finalise pas le commit de merge
    - Les fichiers en erreur contiennent les deux possibilités
    - L’utilisateur édite les fichiers pour garder la bonne version du code
    - L’utilisateur commit tous les fichiers modifiés
    - **Le merge est finalisé**


## Fusion avec conflit (4/4)

Regardez le contenu du fichier `LICENSE` maintenant:
``` shell
$ cat LICENSE
<<<<<<< HEAD
modifié dans master
=======
modifié dans conflit
>>>>>>> conflit
```

Vous pouvez choisir de garder l'une ou l'autre version (`HEAD` ou `conflit`) puis commit. Editez le fichier `LICENSE` en supprimant les lignes possédants des marqueurs de fusion (`<<<<<<<` , `=======` , `>>>>>>>` )

``` shell
$ cat LICENSE
modifié dans conflit
```

Puis ajoutez et validez les modifications:

``` shell
$ git add LICENSE
$ git commit -m "Résolution du conflit sur LICENSE"
```

## Gestion des tags

Un tag permet
(comme son nom l'indique) de taguer un commit particulier. Plutôt que
d'utiliser un numéro de commit pour réaliser un `git checkout` dessus, il
est plus agréable de tager une commit, par un numéro de version par
exemple.

<br><br>

tager votre branche avec la ligne suivante :

``` shell
$ git tag -a v0.1 -m "premier tag"
```

Faire plusieurs commit, puis

``` shell
$ git tag -a v1 -m "1ère version fonctionnelle de mon code"
```

La ligne suivante permet de retourner facilement à une version de votre
dépôt

``` shell
$ git checkout v1 # ou v0.1
```

## Exemple d'utilisation des tags

Dans un dossier autre que celui de votre dépot git. 
Pour récupérer un dépot sur un serveur :

``` shell
$ git clone https://gitlab.inria.fr/formation/exemple_got_depot.git
```

Parcourez le dossier pour voir les différents fichier. 
Utilisez `git log` pour voir les tags disponible puis faite un `git checkout`

``` shell
$ git checkout vXXX
```

## Une commande un peu plus avancée : `git stash` (2/3)

<div class="alert alert-info">
<h4><i class="fa fa-info"></i>Et si&#x2026;</h4>
Et si je souhaite mettre de côté des modifications sans pour autant les appliquer, pour travailler sur une autre partie du projet ?

$\implies$ `git stash`
</div>

1.  Créez deux nouveaux fichiers (`fichier_indexé.txt`, et `nouveau_fichier.txt`),
2.  Indexez `fichier_indexé.txt`
3.  Faites maintenant un `git status` pour voir l'état de votre dépôt:

```shell
$ git status
On branch main
Changes to be committed:

   new file:   fichier_indexé.txt
   
Changes not staged for commit:
   
   modified:   nouveau_fichier.txt
```

## Une commande un peu plus avancée : `git stash` (2/3)

Maintenant, faites `git stash`:
``` shell
$ git stash
Saved working directory and index state WIP on main: 5002d47 our new homepage
HEAD is now at 5002d47 our new homepage
```

À nouveau `git status`:

``` shell
$ git status
On branch main
nothing to commit, working tree clean
```

<div class="alert alert-success">
<h4><i class="fa fa-check"></i>Note</h4>
À ce stade, vous êtes libre d'apporter des modifications, de créer de nouveaux commits, de changer de branches et d'effectuer toute autre opération Git; puis revenez et réappliquez votre stash quand vous serez prêt.
</div>


## Une commande un peu plus avancée : `git stash` (3/3)

Pour rappliquer le code mis de côté :

- `git stash pop` : réapplique les modifications du `stash` et les supprime du `stash`
- `git stash apply` : réapplique les modifications du `stash` **sans** les supprimer du `stash` (pratique pour appliquer les même modifications sur plusieurs branches !)

    $ git stash apply
    On branch main
    Changes to be committed:
    
        new file:   style.css
    
    Changes not staged for commit:
    
        modified:   index.html


## Et bien d'autres commandes !

- `git diff`
- `git stash`
- `git reset`
- `git rebase`
- *etc.*


# Utiliser une interface graphique

<div class="container">
<div class="col">

![](img/git-gui.png "capture d'écran de git-gui")

</div>
<div class="col">

![](img/gitk.png "capture d'écran de gitk")

</div>
</div>


# Travail collaboratif


## Particularité git

![](img/network.png)


## Cloner un répertoire distant

Pour télécharger et synchroniser avec un dépôt distant : 

```shell
    git clone git@gitlab.inria.fr:formation/exemple_got_depot.git
```

Pour pousser vos modifications : 

```shell
    git push
```

Pour télécharger les modifications : 

```shell
    git pull
```

<div class="alert alert-warning">
<h4><i class="fa fa-warning"></i>Conflit !</h4>
Dans le cas où un `pull` risque d'effacer vos modifications locales, `git` vous préviens et annule ! Ouf ! 😉
</div>


# Des questions ?

