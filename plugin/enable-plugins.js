// Chalkboard needs customcontrols plugin
options['customcontrols'] = {
	controls: [
	    { icon: '<i class="fa fa-pen-square"></i>',
              title: 'Toggle chalkboard (B)',
              action: 'RevealChalkboard.toggleChalkboard();'
	    },
	    { icon: '<i class="fa fa-pen"></i>',
              title: 'Toggle notes canvas (C)',
              action: 'RevealChalkboard.toggleNotesCanvas();'
	    }
	]
};

// RevealMenu
options['menu'] = {
    themes: true,
    themesPath: '_assets/node_modules/reveal.js/dist/theme/'
}

// CopyCode
options['copycode'] = {
    button: "hover",
    display: "both",
    text: {
        copy: "Copy",
        copied: "Copied!",
    },
    plaintextonly: true,
    timeout: 1000,
    style: {
        copybg: "orange",
        copiedbg: "green",
        copycolor: "black",
        copiedcolor: "white",
        copyborder: "",
        copiedborder: "",
        scale: 1,
        offset: 0,
        radius: 0
    },
    tooltip: true,
    iconsvg: {
        copy: '',
        copied: ''
    },
    csspath: "",
    clipboardjspath: ""
}

// Add the plugins
options.plugins.push(
    RevealMenu,
    RevealChalkboard,
    RevealCustomControls,
    CopyCode,
    RevealMermaid
);
