# Git-training

## Introduction 

Ici se trouve les supports d'une introduction à git (version française uniquement pour l'instant :flag_france:) !

Les objectifs de la formation sur une demi-journée sont : 

-   Comprendre ce qu'est un logiciel de contrôle de version,
-   Mettre votre travail sous contrôle de version
-   Apprendre à utiliser les fonctionnalités de base de Git,
-   Comprendre le principe d'une forge logicielle.
-   Commencer à travailler en collaboration


## Version en ligne

Les diapositives sont consultables en ligne via le lien suivant : [version en ligne](https://ttoullie.gitlabpages.inria.fr/git-training/).

## Sources

Vous pouvez aussi télécharger les sources de la présentation si vous voulez contribuer : 

``` shell
git clone https://gitlab.inria.fr/ttoullie/git-training.git
cd git-training
```

Vous avez besoin de [NodeJS](https://nodejs.org/en). Une fois installé, avec `npm`:

``` shell
npm install
npm run serve
```

### Commandes

``` shell
serve # Permets de créer un serveur qui met à jour les diapos automatiquement
  reveal-md presentation.md -w
export:site # Exporter localement dans le sous-dossier `./public`
  reveal-md presentation.md --static public
export:pdf # Exporter au format pdf (à fixer)
  reveal-md 2print.md --print presentation.pdf
```




