#!/bin/sh
#--- This script aims at providing the right
#---   asset files at the right place
# copy all basic assets
cp img/* public/img/
cp css/*.css public/css/

# copy font-awesome-4.7.0 for in-use
cp -R css/font-awesome-4.7.0 public/css/
cp -R css/font-awesome-4.7.0/fonts public/_assets/css/font-awesome-4.7.0

# copy assets for reveal.js-menu
cp node_modules/reveal.js-menu/menu.css public/_assets/node_modules/reveal.js-menu/
mkdir -p public/_assets/node_modules/reveal.js-menu/font-awesome/css
cp node_modules/reveal.js-menu/font-awesome/css/all.css public/_assets/node_modules/reveal.js-menu/font-awesome/css/all.css
mkdir -p public/_assets/node_modules/reveal.js-menu/font-awesome/
cp -R node_modules/reveal.js-menu/font-awesome/webfonts public/_assets/node_modules/reveal.js-menu/font-awesome/webfonts
mkdir -p public/_assets/node_modules/reveal.js/dist/
cp -R node_modules/reveal.js/dist/theme public/_assets/node_modules/reveal.js/dist/theme

# copy assets for chalkboard
mkdir -p public/_assets/node_modules/reveal.js-plugins/chalkboard/
 cp -R node_modules/reveal.js-plugins/chalkboard/img public/_assets/node_modules/reveal.js-plugins/chalkboard/img
