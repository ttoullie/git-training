// headings of level 1 (e.g., '# Heading 1`) trigger a new horizontal slide
// headings of level 2 (e.g., '## Heading 2`) trigger a new vertical slide
module.exports = (markdown, options) => {
    return new Promise((resolve, reject) => {
	var first_header = true;
	var mermaid_block = false;
	return resolve(
	    markdown
		.split('\n')
		.map((line, index) => {
		    if(mermaid_block && /^```/.test(line)){
			mermaid_block = false;
			return "</div>\n";
		    }
		    if(/^```[ ]*mermaid/.test(line)){
			mermaid_block = true;
			return "<div class=\"mermaid\">\n";
		    }
		    if (/^<div/.test(line)){
			return line + "\n\n";
		    }
		    if(/^<\/div/.test(line)){
			return "\n\n" + line;
		    }
		    if (!/^#/.test(line) || index === 0) return line;

		    const is_horizontal = /^#\ /.test(line);
		    const is_vertical = /^##\ /.test(line);
		    if (is_horizontal) {
			if(first_header){
			    first_header = false;
			}else{
			    return '\n---\n\n' + line;
			}
		    }
		    if (is_vertical) {
			return '\n----\n\n' + line;
		    }
		    return line;
		})
		.join('\n')
	);
    });
};
